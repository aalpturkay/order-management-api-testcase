package com.aalpturkay.ordermanagementapi.log.info.converter;

import com.aalpturkay.ordermanagementapi.log.info.dto.LogInfoSaveRequest;
import com.aalpturkay.ordermanagementapi.log.info.entity.LogInfo;
import org.springframework.stereotype.Component;


@Component
public class LogInfoConverter {

    public LogInfo convertToLogInfo(LogInfoSaveRequest logInfoSaveRequest){
        LogInfo logInfo = new LogInfo();

        logInfo.setCrudType(logInfoSaveRequest.getCrudType());
        logInfo.setEntityType(logInfoSaveRequest.getEntityType());

        return logInfo;
    }
}
