package com.aalpturkay.ordermanagementapi.log.info.service;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntityService;
import com.aalpturkay.ordermanagementapi.log.info.entity.LogInfo;
import com.aalpturkay.ordermanagementapi.log.info.repository.LogInfoRepository;
import org.springframework.stereotype.Service;

@Service
public class LogInfoEntityService extends BaseEntityService<LogInfo, LogInfoRepository> {
    public LogInfoEntityService(LogInfoRepository repository) {
        super(repository);
    }
}
