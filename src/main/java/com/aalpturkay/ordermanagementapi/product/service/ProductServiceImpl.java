package com.aalpturkay.ordermanagementapi.product.service;

import com.aalpturkay.ordermanagementapi.common.exception.ItemNotFoundException;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.customerorder.service.CustomerOrderEntityService;
import com.aalpturkay.ordermanagementapi.log.common.LogService;
import com.aalpturkay.ordermanagementapi.log.info.dto.LogInfoSaveRequest;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoCrudType;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoEntityType;
import com.aalpturkay.ordermanagementapi.product.converter.ProductConverter;
import com.aalpturkay.ordermanagementapi.product.dto.OrderProductSaveRequest;
import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import com.aalpturkay.ordermanagementapi.product.dto.ProductSaveRequest;
import com.aalpturkay.ordermanagementapi.product.dto.ProductUpdateQuantityRequest;
import com.aalpturkay.ordermanagementapi.product.entity.Product;
import com.aalpturkay.ordermanagementapi.product.enums.EnumProductErrorMessage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductEntityService productEntityService;
    private final ProductConverter productConverter;
    private final CustomerOrderEntityService customerOrderEntityService;
    private final LogService<LogInfoSaveRequest> logService;

    public ProductServiceImpl(ProductEntityService productEntityService,
                              ProductConverter productConverter,
                              CustomerOrderEntityService customerOrderEntityService, LogService<LogInfoSaveRequest> logService) {
        this.productEntityService = productEntityService;

        this.productConverter = productConverter;
        this.customerOrderEntityService = customerOrderEntityService;
        this.logService = logService;
    }


    @Override
    public void save(ProductSaveRequest productSaveRequest) {
        Product product = new Product();

        product.setQuantity(productSaveRequest.getQuantity());
        product.setDescription(productSaveRequest.getDescription());
        product.setBarcode(productSaveRequest.getBarcode());
        product.setPrice(productSaveRequest.getPrice());
        product.setCustomerOrder(customerOrderEntityService.
                findByIdWithControl(productSaveRequest.getCustomerOrderId()));

        productEntityService.save(product);

        LogInfoSaveRequest logInfoSaveRequest = LogInfoSaveRequest.builder()
                .crudType(EnumLogInfoCrudType.SAVE)
                .entityType(EnumLogInfoEntityType.PRODUCT)
                .build();

        logService.saveAndLog(logInfoSaveRequest);

        productConverter.convertToProductResponse(product);
    }

    @Override
    public ProductResponse updateProductQuantity(UUID productId,
                                                 ProductUpdateQuantityRequest productUpdateQuantityRequest) {
        Product product = getProductByIdWithControl(productId);
        product.setQuantity(productUpdateQuantityRequest.getQuantity());
        productEntityService.save(product);

        LogInfoSaveRequest logInfoSaveRequest = LogInfoSaveRequest.builder()
                .crudType(EnumLogInfoCrudType.UPDATE)
                .entityType(EnumLogInfoEntityType.PRODUCT)
                .build();

        logService.saveAndLog(logInfoSaveRequest);

        return productConverter.convertToProductResponse(product);
    }

    @Override
    public List<ProductResponse> saveAll(List<OrderProductSaveRequest> productSaveRequestList,
                                         CustomerOrder customerOrder) {
        List<ProductResponse> productResponseList = new ArrayList<>();

        productSaveRequestList.forEach(productSaveRequest -> {
            Product product = new Product();
            product.setCustomerOrder(customerOrder);
            product.setDescription(productSaveRequest.getDescription());
            product.setBarcode(productSaveRequest.getBarcode());
            product.setPrice(productSaveRequest.getPrice());
            product.setQuantity(productSaveRequest.getQuantity());
            productEntityService.save(product);

            productResponseList.add(productConverter.convertToProductResponse(product));
        });

        return productResponseList;
    }

    @Override
    public List<ProductResponse> findAllByCustomerOrder(CustomerOrder customerOrder) {
        List<Product> products = productEntityService.findAllByCustomerOrder(customerOrder);

        LogInfoSaveRequest logInfoSaveRequest = LogInfoSaveRequest.builder()
                .crudType(EnumLogInfoCrudType.GET)
                .entityType(EnumLogInfoEntityType.PRODUCT)
                .build();

        logService.saveAndLog(logInfoSaveRequest);

        return productConverter.convertToProductResponseList(products);
    }

    @Override
    public void deleteById(UUID productId) {
        Product product = getProductByIdWithControl(productId);
        productEntityService.delete(product);

        LogInfoSaveRequest logInfoSaveRequest = LogInfoSaveRequest.builder()
                .crudType(EnumLogInfoCrudType.DELETE)
                .entityType(EnumLogInfoEntityType.PRODUCT)
                .build();

        logService.saveAndLog(logInfoSaveRequest);
    }

    @Override
    public List<ProductResponse> findAllByCustomerOrderAndQuantity(UUID customerOrderId, Integer min, Integer max) {
        List<Product> productList;
        CustomerOrder customerOrder = customerOrderEntityService.findByIdWithControl(customerOrderId);

        if (min == null && max == null){
            productList = productEntityService.findAllByCustomerOrder(customerOrder);
            return productConverter.convertToProductResponseList(productList);
        }

        if (min == null){
            productList = productEntityService.findAllSmallerQuantity(max, customerOrder);
            return productConverter.convertToProductResponseList(productList);
        }

        if (max == null) {
            productList = productEntityService.findAllGreaterQuantity(min, customerOrder);
            return productConverter.convertToProductResponseList(productList);
        }

        productList = productEntityService.findAllBetweenQuantityByCustomerOrder(min, max, customerOrder);
        return productConverter.convertToProductResponseList(productList);
    }

    private Product getProductByIdWithControl(UUID productId) {
        LogInfoSaveRequest logInfoSaveRequest = LogInfoSaveRequest.builder()
                .crudType(EnumLogInfoCrudType.GET)
                .entityType(EnumLogInfoEntityType.PRODUCT)
                .build();

        logService.saveAndLog(logInfoSaveRequest);

        return productEntityService.findById(productId).orElseThrow(() ->
                new ItemNotFoundException(EnumProductErrorMessage.PRODUCT_NOT_FOUND));
    }
}
