package com.aalpturkay.ordermanagementapi.log.common;

public interface LogService<T extends LogSaveDto> {
    void saveAndLog(T logSaveDto);
}
