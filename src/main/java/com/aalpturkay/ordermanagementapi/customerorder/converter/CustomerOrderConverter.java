package com.aalpturkay.ordermanagementapi.customerorder.converter;

import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderResponse;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerOrderConverter {

    public CustomerOrder convertToCustomerOrder(Customer customer){
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setCustomer(customer);
        customerOrder.setDeliveryAddress(customer.getAddress());

        return customerOrder;
    }

    public CustomerOrderResponse convertToCustomerOrderResponse(CustomerOrder customerOrder, List<ProductResponse> productResponseList){
        CustomerOrderResponse customerOrderResponse = new CustomerOrderResponse();

        customerOrderResponse.setId(customerOrder.getId());
        customerOrderResponse.setCustomerName(customerOrder.getCustomer().getName());
        customerOrderResponse.setDeliveryAddress(customerOrder.getDeliveryAddress());
        customerOrderResponse.setProducts(productResponseList);

        return customerOrderResponse;
    }
}
