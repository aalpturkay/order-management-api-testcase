package com.aalpturkay.ordermanagementapi.common.exception;

import com.aalpturkay.ordermanagementapi.common.base.BaseErrorMessage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends RuntimeException{
    private final BaseErrorMessage baseErrorMessage;

    public BusinessException(BaseErrorMessage baseErrorMessage) {
        this.baseErrorMessage = baseErrorMessage;
    }
}
