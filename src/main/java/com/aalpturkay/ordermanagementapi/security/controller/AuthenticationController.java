package com.aalpturkay.ordermanagementapi.security.controller;

import com.aalpturkay.ordermanagementapi.common.response.GenResponse;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerResponse;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerSaveRequest;
import com.aalpturkay.ordermanagementapi.security.dto.LoginRequest;
import com.aalpturkay.ordermanagementapi.security.service.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Operation(summary = "Register to API as a customer.",
            description = "### Requirements\n- *Username should be unique*\n\n" +
                    "### Sample Input\n\n" + "```\n" + "{\n" +
                    "  \"name\": \"Alp Turkay\",\n" +
                    "  \"address\": \"my address\",\n" +
                    "  \"username\": \"alpturkay\",\n" +
                    "  \"password\": \"password\"\n" +
                    "}" + "\n```\n\n" + "### Sample Output\n\n" + "```\n" + "{\n" +
                    "  \"data\": {\n" +
                    "    \"id\": 3fa85f64-5717-4562-b3fc-2c963f66afa6,\n" +
                    "    \"name\": \"Alp Turkay\",\n" +
                    "    \"username\": \"alpturkay\",\n" +
                    "    \"address\": \"my address\"\n" +
                    "  }\n" +
                    "  \"success\": true\n" +
                    "  \"messages\": null\n" +
                    "}" + "\n```\n\n*Note: Passwords are stored as hashes not raw.*",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "201", description = "Customer successfully registered.")
            }


    )
    @PostMapping("/register")
    public ResponseEntity<GenResponse<CustomerResponse>> register(@RequestBody CustomerSaveRequest customerSaveRequest){
        CustomerResponse customerResponse = authenticationService.register(customerSaveRequest);

        return ResponseEntity.ok(GenResponse.of(customerResponse));
    }

    @Operation(summary = "Login to get a bearer access token.",
            description = "### Requirements\n- *Credentials must be match with the DB.*\n\n" +
                    "### Sample Input\n\n" + "```\n" + "{\n" +
                    "  \"username\": \"alpturkay\",\n" +
                    "  \"password\": \"password\"\n" +
                    "}" + "\n```\n\n" + "### Sample Output\n\n" + "```\n" + "{\n" +
                    "  \"data\": \"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdHJpbmciLCJpYXQiOjE2NjcxMzA4NjgsImV4cCI6MTY2NzIxNzI2OH0.4sqiyR-l1uw3Xcx2V_qp7qAacHLt2qosBvCidlEf8a5hcA8mwuEA_Hd9Pi44en2BPGe8pytBL0E2dVYuZseaxw\",\n" +
                    "  \"success\": true\n" +
                    "  \"messages\": null,\n" +
                    "}" + "\n```\n\n*Note: Use returned bearer JWT access token to authorize.*",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "200", description = "Successfully logged in."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "500", description = "Bad credentials."),
            }
    )
    @PostMapping("/login")
    public ResponseEntity<GenResponse<String>> login(@RequestBody LoginRequest loginRequest){
        String login = authenticationService.login(loginRequest);

        return ResponseEntity.ok(GenResponse.of(login));
    }
}
