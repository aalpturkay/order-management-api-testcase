package com.aalpturkay.ordermanagementapi.log.error.entity;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.http.HttpStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "LOG_ERROR")
@Getter
@Setter
public class LogError extends BaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "ID_CUSTOMER")
    private UUID customerId;

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    private HttpStatus httpStatus;

    @Column(name = "BODY", length = 4000)
    private String body;

    @Column(name = "HEADERS", length = 4000)
    private String headers;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ERROR_DATE")
    private Date errorDate;
}
