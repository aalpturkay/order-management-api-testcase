package com.aalpturkay.ordermanagementapi.log.info.dto;

import com.aalpturkay.ordermanagementapi.log.common.LogSaveDto;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoCrudType;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoEntityType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class LogInfoSaveRequest extends LogSaveDto {
    private EnumLogInfoCrudType crudType;
    private EnumLogInfoEntityType entityType;
}
