package com.aalpturkay.ordermanagementapi.product.entity;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntity;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "PRODUCTS")
@Getter
@Setter
public class Product extends BaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "BARCODE")
    private String barcode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "PRICE", precision = 19, scale = 2)
    private BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerOrder customerOrder;
}
