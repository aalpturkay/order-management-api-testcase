package com.aalpturkay.ordermanagementapi.customer.service;

import com.aalpturkay.ordermanagementapi.common.exception.BusinessException;
import com.aalpturkay.ordermanagementapi.common.exception.ItemNotFoundException;
import com.aalpturkay.ordermanagementapi.customer.converter.CustomerConverter;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerResponse;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerSaveRequest;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customer.enums.EnumCustomerErrorMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerEntityService customerEntityService;
    private final PasswordEncoder passwordEncoder;
    private final CustomerConverter customerConverter;

    public CustomerServiceImpl(CustomerEntityService customerEntityService, PasswordEncoder passwordEncoder, CustomerConverter customerConverter) {
        this.customerEntityService = customerEntityService;
        this.passwordEncoder = passwordEncoder;
        this.customerConverter = customerConverter;
    }

    @Override
    public CustomerResponse save(CustomerSaveRequest customerSaveRequest) {
        if (customerEntityService.existsByUsername(customerSaveRequest.getUsername())){
            throw new BusinessException(EnumCustomerErrorMessage.DUPLICATE_USERNAME);
        }

        Customer customer = customerConverter.convertToCustomer(customerSaveRequest);
        customer.setUsername(customerSaveRequest.getUsername());
        String hash = passwordEncoder.encode(customerSaveRequest.getPassword());
        customer.setPassword(hash);

        customerEntityService.save(customer);

        return customerConverter.convertToCustomerResponse(customer);
    }

    @Override
    public Customer findById(UUID id) {
        return customerEntityService.findById(id).
                orElseThrow(() -> new ItemNotFoundException(EnumCustomerErrorMessage.CUSTOMER_NOT_FOUND));
    }
}
