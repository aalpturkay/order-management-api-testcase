package com.aalpturkay.ordermanagementapi.log.error.repository;

import com.aalpturkay.ordermanagementapi.log.error.entity.LogError;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LogErrorRepository extends JpaRepository<LogError, UUID> {
}
