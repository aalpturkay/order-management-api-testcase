package com.aalpturkay.ordermanagementapi.product.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
public class ProductUpdateQuantityRequest {
    @NotNull(message = "Quantity must be provided!")
    @PositiveOrZero(message = "Quantity must be positive or zero!")
    private int quantity;
}
