package com.aalpturkay.ordermanagementapi.log.error.service;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntityService;
import com.aalpturkay.ordermanagementapi.log.error.entity.LogError;
import com.aalpturkay.ordermanagementapi.log.error.repository.LogErrorRepository;
import org.springframework.stereotype.Service;

@Service
public class LogErrorEntityService extends BaseEntityService<LogError, LogErrorRepository> {
    public LogErrorEntityService(LogErrorRepository repository) {
        super(repository);
    }
}
