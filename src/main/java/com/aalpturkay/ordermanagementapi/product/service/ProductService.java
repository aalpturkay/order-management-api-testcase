package com.aalpturkay.ordermanagementapi.product.service;

import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.product.dto.OrderProductSaveRequest;
import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import com.aalpturkay.ordermanagementapi.product.dto.ProductSaveRequest;
import com.aalpturkay.ordermanagementapi.product.dto.ProductUpdateQuantityRequest;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    void save(ProductSaveRequest productSaveRequest);
    ProductResponse updateProductQuantity(UUID productId, ProductUpdateQuantityRequest productUpdateQuantityRequest);
    List<ProductResponse> saveAll(List<OrderProductSaveRequest> productSaveRequestList, CustomerOrder customerOrder);

    List<ProductResponse> findAllByCustomerOrder(CustomerOrder customerOrder);
    void deleteById(UUID productId);

    List<ProductResponse> findAllByCustomerOrderAndQuantity(UUID customerOrderId, Integer min, Integer max);
}
