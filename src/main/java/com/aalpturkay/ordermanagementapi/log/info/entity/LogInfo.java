package com.aalpturkay.ordermanagementapi.log.info.entity;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntity;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoCrudType;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoEntityType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "LOG_INFO")
@Getter
@Setter
public class LogInfo extends BaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "ID_CUSTOMER")
    private UUID customerId;

    @Enumerated(EnumType.STRING)
    @Column(name = "CRUD_TYPE")
    private EnumLogInfoCrudType crudType;

    @Enumerated(EnumType.STRING)
    @Column(name = "ENTITY_TYPE")
    private EnumLogInfoEntityType entityType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INFO_DATE")
    private Date infoDate;
}
