package com.aalpturkay.ordermanagementapi.customer.converter;

import com.aalpturkay.ordermanagementapi.customer.dto.CustomerResponse;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerSaveRequest;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerConverter {

    public Customer convertToCustomer(CustomerSaveRequest customerSaveRequest){
        Customer customer = new Customer();

        customer.setAddress(customerSaveRequest.getAddress());
        customer.setName(customerSaveRequest.getName());

        return customer;
    }

    public CustomerResponse convertToCustomerResponse(Customer customer){
        CustomerResponse customerResponse = new CustomerResponse();

        customerResponse.setId(customer.getId());
        customerResponse.setUsername(customer.getUsername());
        customerResponse.setName(customer.getName());
        customerResponse.setAddress(customer.getAddress());

        return customerResponse;
    }
}
