package com.aalpturkay.ordermanagementapi.customerorder.dto;

import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class CustomerOrderResponse {
    private UUID id;
    private String customerName;
    private String deliveryAddress;
    private List<ProductResponse> products;
}
