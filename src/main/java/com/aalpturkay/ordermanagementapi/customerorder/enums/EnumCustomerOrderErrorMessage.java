package com.aalpturkay.ordermanagementapi.customerorder.enums;

import com.aalpturkay.ordermanagementapi.common.base.BaseErrorMessage;

public enum EnumCustomerOrderErrorMessage implements BaseErrorMessage {
    ORDER_NOT_FOUND("Order not found!")
    ;

    private final String message;

    EnumCustomerOrderErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
