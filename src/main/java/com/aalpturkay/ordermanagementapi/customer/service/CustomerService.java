package com.aalpturkay.ordermanagementapi.customer.service;

import com.aalpturkay.ordermanagementapi.customer.dto.CustomerResponse;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerSaveRequest;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;

import java.util.UUID;

public interface CustomerService {
    CustomerResponse save(CustomerSaveRequest customerSaveRequest);
    Customer findById(UUID id);
}
