package com.aalpturkay.ordermanagementapi.product.converter;

import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import com.aalpturkay.ordermanagementapi.product.entity.Product;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductConverter {
    public ProductResponse convertToProductResponse(Product product){
        ProductResponse productResponse = new ProductResponse();

        productResponse.setProductId(product.getId());
        productResponse.setBarcode(product.getBarcode());
        productResponse.setDescription(product.getDescription());
        productResponse.setPrice(product.getPrice());
        productResponse.setQuantity(product.getQuantity());

        return productResponse;
    }

    public List<ProductResponse> convertToProductResponseList(List<Product> products){
        return products.stream().map(this::convertToProductResponse).collect(Collectors.toList());
    }
}
