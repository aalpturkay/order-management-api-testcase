package com.aalpturkay.ordermanagementapi.customerorder.service;

import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderResponse;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderSaveRequest;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderUpdateDeliveryAddressRequest;

import java.util.List;
import java.util.UUID;

public interface CustomerOrderService {
    CustomerOrderResponse save(CustomerOrderSaveRequest saveRequest, UUID customerId);
    List<CustomerOrderResponse> findAll(UUID customerId);
    CustomerOrderResponse findById(UUID customerOrderId);
    void deleteById(UUID customerOrderId);
    CustomerOrderResponse updateDeliveryAddress(UUID customerOrderId, CustomerOrderUpdateDeliveryAddressRequest customerOrderUpdateDeliveryAddressRequest);
}
