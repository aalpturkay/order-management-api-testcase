package com.aalpturkay.ordermanagementapi.customer.dto;

import lombok.Data;

@Data
public class CustomerSaveRequest {
    private String name;
    private String address;
    private String username;
    private String password;
}
