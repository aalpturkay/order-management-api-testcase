package com.aalpturkay.ordermanagementapi.customerorder.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CustomerOrderUpdateDeliveryAddressRequest {
    @NotNull(message = "Delivery address must be provided!")
    private String deliveryAddress;
}
