package com.aalpturkay.ordermanagementapi.product.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
public class OrderProductSaveRequest {
    @NotNull(message = "Barcode must be provided!")
    private String barcode;
    @NotNull(message = "Description must be provided!")
    private String description;
    @NotNull(message = "Quantity must be provided!")
    @PositiveOrZero(message = "Quantity must be positive or zero!")
    private int quantity;
    @NotNull(message = "Price must be provided!")
    @PositiveOrZero(message = "Price must be positive or zero!")
    private BigDecimal price;
}
