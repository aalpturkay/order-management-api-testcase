# Order Management Restful Api

**Manage orders by creating new ones with products, add products to orders, update delivery addresses and product's quantities and search products by their quantities.**

# SwaggerUI Docs

### All Endpoints

![Alt text](./src/main/resources/static/images/endpoints.png)

### Authentication

![Alt text](./src/main/resources/static/images/register.png)

![Alt text](./src/main/resources/static/images/login.png)

### Create Order

![Alt text](./src/main/resources/static/images/orders-save-1.png)
![Alt text](./src/main/resources/static/images/orders-save-2.png)

### Delete Order

![Alt text](./src/main/resources/static/images/order-delete.png)

### Get Order

![Alt text](./src/main/resources/static/images/get-orders.png)
![Alt text](./src/main/resources/static/images/order-get-1.png)

### Update Order Delivery Address

![Alt text](./src/main/resources/static/images/order-address-1.png)
![Alt text](./src/main/resources/static/images/order-address-2.png)

### Add Product to Order

![Alt text](./src/main/resources/static/images/product-save.png)

### Search Product

![Alt text](./src/main/resources/static/images/product-search.png)

### Delete Product from an Order

![Alt text](./src/main/resources/static/images/product-delete.png)

### Update Product Quantity

![Alt text](./src/main/resources/static/images/product-update.png)

### Log Mechanism
* Api holds logs as error and info.
* Info logs can be any CRUD type like DELETE
* Info log also saves which data type requested and when

![Alt text](./src/main/resources/static/images/logs.png)