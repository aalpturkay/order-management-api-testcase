package com.aalpturkay.ordermanagementapi.security.enums;

public enum EnumAuthConstant {
    BEARER("Bearer ")
    ;

    private final String constant;

    EnumAuthConstant(String constant) {
        this.constant = constant;
    }

    public String getConstant() {
        return constant;
    }

    @Override
    public String toString() {
        return constant;
    }
}
