package com.aalpturkay.ordermanagementapi.customer.enums;

import com.aalpturkay.ordermanagementapi.common.base.BaseErrorMessage;

public enum EnumCustomerErrorMessage implements BaseErrorMessage {
    DUPLICATE_USERNAME("This username is already exists!"),
    CUSTOMER_NOT_FOUND("Customer not found!"),
    ;
    private final String message;

    EnumCustomerErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
