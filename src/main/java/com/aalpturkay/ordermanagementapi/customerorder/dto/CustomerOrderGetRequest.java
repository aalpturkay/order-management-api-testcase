package com.aalpturkay.ordermanagementapi.customerorder.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CustomerOrderGetRequest {
    private UUID customerId;
}
