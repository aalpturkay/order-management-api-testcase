package com.aalpturkay.ordermanagementapi.customer.entity;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntity;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "CUSTOMERS")
@Getter
@Setter
public class Customer extends BaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "NAME", length = 100)
    private String name;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "USERNAME", unique = true)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CustomerOrder> customerOrderList;
}
