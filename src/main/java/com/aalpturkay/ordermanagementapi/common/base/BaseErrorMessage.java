package com.aalpturkay.ordermanagementapi.common.base;

public interface BaseErrorMessage {
    String getMessage();
}
