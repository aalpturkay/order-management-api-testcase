package com.aalpturkay.ordermanagementapi.log.info.enums;

public enum EnumLogInfoCrudType {
    SAVE,
    GET,
    UPDATE,
    DELETE
    ;
}
