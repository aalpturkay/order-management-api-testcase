package com.aalpturkay.ordermanagementapi.product.enums;

import com.aalpturkay.ordermanagementapi.common.base.BaseErrorMessage;

public enum EnumProductErrorMessage implements BaseErrorMessage {
    PRODUCT_NOT_FOUND("Product not found!")
    ;

    private final String message;
    EnumProductErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
