package com.aalpturkay.ordermanagementapi.common.base;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntity;
import com.aalpturkay.ordermanagementapi.common.exception.EnumErrorMessage;
import com.aalpturkay.ordermanagementapi.common.exception.ItemNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public abstract class BaseEntityService<E extends BaseEntity, R extends JpaRepository<E, UUID>> {
    private final R repository;

    public BaseEntityService(R repository) {
        this.repository = repository;
    }

    public List<E> findAll(){
        return repository.findAll();
    }

    public Optional<E> findById(UUID id){
        return repository.findById(id);
    }

    public E findByIdWithControl(UUID id){
        Optional<E> optionalEntity = repository.findById(id);

        E entity;
        if (optionalEntity.isPresent()){
            entity = optionalEntity.get();
        } else {
            throw new ItemNotFoundException(EnumErrorMessage.ITEM_NOT_FOUND);
        }

        return entity;
    }

    public E save(E entity){
        return repository.save(entity);
    }

    public void delete(E e){
        repository.delete(e);
    }

    public void deleteById(UUID id){
        repository.deleteById(id);
    }

    public boolean existsById(UUID id){
        return repository.existsById(id);
    }

    public R getRepository(){
        return repository;
    }
}
