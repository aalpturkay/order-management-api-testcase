package com.aalpturkay.ordermanagementapi.common.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GenResponse<T> implements Serializable {
    private T data;
    private boolean success;
    private String message;

    public GenResponse(T data, boolean success) {
        this.data = data;
        this.success = success;
    }

    public static <T> GenResponse<T> of(T t){
        return new GenResponse<>(t, true);
    }

    public static <T> GenResponse<T> error(T t){
        return new GenResponse<>(t, false);
    }

    public static <T> GenResponse<T> empty(){
        return new GenResponse<>(null, true);
    }

    public void setMessages(String message) {
        this.message = message;
    }
}
