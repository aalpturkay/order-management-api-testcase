package com.aalpturkay.ordermanagementapi.customer.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class CustomerResponse {
    private UUID id;
    private String name;
    private String username;
    private String address;
}
