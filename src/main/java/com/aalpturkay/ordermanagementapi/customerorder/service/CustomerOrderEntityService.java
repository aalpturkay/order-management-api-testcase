package com.aalpturkay.ordermanagementapi.customerorder.service;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntityService;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.customerorder.repository.CustomerOrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerOrderEntityService extends BaseEntityService<CustomerOrder, CustomerOrderRepository> {

    public CustomerOrderEntityService(CustomerOrderRepository repository) {
        super(repository);
    }

    public List<CustomerOrder> findAllByCustomer(Customer customer) {
        return getRepository().findAllByCustomer(customer);
    }
}
