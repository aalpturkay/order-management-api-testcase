package com.aalpturkay.ordermanagementapi.product.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class ProductResponse {
    private UUID productId;
    private String barcode;
    private String description;
    private int quantity;
    private BigDecimal price;
}
