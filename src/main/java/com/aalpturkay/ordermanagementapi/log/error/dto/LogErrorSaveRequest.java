package com.aalpturkay.ordermanagementapi.log.error.dto;

import com.aalpturkay.ordermanagementapi.log.common.LogSaveDto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Builder
public class LogErrorSaveRequest extends LogSaveDto {
    private HttpStatus httpStatus;
    private String body;
    private String headers;
}
