package com.aalpturkay.ordermanagementapi.security.service;

import com.aalpturkay.ordermanagementapi.customer.dto.CustomerResponse;
import com.aalpturkay.ordermanagementapi.customer.dto.CustomerSaveRequest;
import com.aalpturkay.ordermanagementapi.customer.service.CustomerService;
import com.aalpturkay.ordermanagementapi.security.config.CustomerUserDetails;
import com.aalpturkay.ordermanagementapi.security.config.TokenGenerator;
import com.aalpturkay.ordermanagementapi.security.dto.LoginRequest;
import com.aalpturkay.ordermanagementapi.security.enums.EnumAuthConstant;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final CustomerService customerService;
    private final TokenGenerator tokenGenerator;

    public AuthenticationService(AuthenticationManager authenticationManager, CustomerService customerService, TokenGenerator tokenGenerator) {
        this.authenticationManager = authenticationManager;
        this.customerService = customerService;
        this.tokenGenerator = tokenGenerator;
    }

    public CustomerResponse register(CustomerSaveRequest customerSaveRequest){
        return customerService.save(customerSaveRequest);
    }

    public String login(LoginRequest loginRequest) {

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(), loginRequest.getPassword()
        );

        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenGenerator.generateJwtToken(authentication);

        return EnumAuthConstant.BEARER.getConstant() + token;
    }

    public static UUID currentUserId(){

        CustomerUserDetails customerUserDetails = getJwtUserDetails();

        return getJwtUserDetailsId(customerUserDetails);
    }

    private static UUID getJwtUserDetailsId(CustomerUserDetails customerUserDetails) {
        UUID userId = null;
        if (customerUserDetails != null){
            userId = customerUserDetails.getId();
        }
        return userId;
    }

    private static CustomerUserDetails getJwtUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        CustomerUserDetails jwtUserDetails = null;
        if (authentication != null && authentication.getPrincipal() instanceof CustomerUserDetails){
            jwtUserDetails = (CustomerUserDetails) authentication.getPrincipal();
        }
        return jwtUserDetails;
    }
}
