package com.aalpturkay.ordermanagementapi.customerorder.repository;

import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;


public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, UUID> {
    List<CustomerOrder> findAllByCustomer(Customer customer);
}
