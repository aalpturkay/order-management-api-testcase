package com.aalpturkay.ordermanagementapi.product.repository;

import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    List<Product> findAllByCustomerOrder(CustomerOrder customerOrder);

    @Query("select a from Product a where (a.customerOrder = :customerOrder and a.quantity between :min and :max)")
    List<Product> findAllBetweenQuantityByCustomerOrder(int min, int max, CustomerOrder customerOrder);

    @Query("select a from Product a where (a.customerOrder = :customerOrder and a.quantity <= :amount)")
    List<Product> findAllSmallerQuantityByCustomerOrder(int amount, CustomerOrder customerOrder);

    @Query("select a from Product a where (a.customerOrder = :customerOrder and a.quantity >= :amount)")
    List<Product> findAllGreaterQuantityByCustomerOrder(int amount, CustomerOrder customerOrder);
}
