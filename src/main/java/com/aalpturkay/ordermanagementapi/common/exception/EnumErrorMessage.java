package com.aalpturkay.ordermanagementapi.common.exception;

import com.aalpturkay.ordermanagementapi.common.base.BaseErrorMessage;

public enum EnumErrorMessage implements BaseErrorMessage {
    ITEM_NOT_FOUND("Item not found!")
    ;

    private final String message;

    EnumErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
