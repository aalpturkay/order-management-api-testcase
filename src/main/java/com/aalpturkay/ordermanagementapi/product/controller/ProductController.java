package com.aalpturkay.ordermanagementapi.product.controller;

import com.aalpturkay.ordermanagementapi.common.response.GenResponse;
import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import com.aalpturkay.ordermanagementapi.product.dto.ProductSaveRequest;
import com.aalpturkay.ordermanagementapi.product.dto.ProductUpdateQuantityRequest;
import com.aalpturkay.ordermanagementapi.product.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Operation(summary = "Add product to an order.",
            description = "### Requirements\n- bearer token must be provided\n\n" +
                    "### Sample Valid Input\n\n" + "```\n" + "{\n" +
                    "  \"customerOrderId\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\",\n" +
                    "  \"barcode\": \"1111111111111\"\n" +
                    "  \"description\": \"Filter Coffee\",\n" +
                    "  \"quantity\": 15,\n" +
                    "  \"price\": 420.99\n" +
                    "}" + "\n```\n\n" +
                    "### Sample Invalid Input\n\n" + "```\n" + "{\n" +
                    "  \"customerOrderId\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\",\n" +
                    "  \"barcode\": \"1111111111111\"\n" +
                    "  \"description\": \"Filter Coffee\",\n" +
                    "  \"quantity\": -15,\n" +
                    "  \"price\": -420.99\n" +
                    "}" + "\n```\n\n",
            responses = {
                    @ApiResponse(content = @Content(mediaType = ""),
                            responseCode = "201", description = "Product added successfully."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "404", description = "Not found."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "400", description = "Validation failed."),
            }
    )
    @PostMapping
    public ResponseEntity<Void> save(@Valid @RequestBody ProductSaveRequest productSaveRequest){
        productService.save(productSaveRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Update the product quantity by product id.",
            description = "### Requirements\n- bearer token must be provided\n\n" +
                    "### Sample Valid Input\n\n" + "```\n" + "{\n" +
                    "  \"quantity\": 20,\n" +
                    "}" + "\n```\n\n" +
                    "### Sample Invalid Input\n\n" + "```\n" + "{\n" +
                    "  \"quantity\": -20,\n" +
                    "}" + "\n```\n\n" +
                    "### Sample Output\n\n" + "```\n" + "{\n" +
                    "  \"data\": {\n" +
                    "    \"productId\": 3fa85f64-5717-4562-b3fc-2c963f66afa6,\n" +
                    "    \"barcode\": \"1111111111111\"\n" +
                    "    \"description\": \"Filter Coffee\",\n" +
                    "    \"quantity\": 20,\n" +
                    "    \"price\": 420.99\n" +
                    "  }\n" +
                    "  \"success\": true\n" +
                    "  \"messages\": null\n" +
                    "}" + "\n```\n\n",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                    ProductResponse.class)),
                            responseCode = "200", description = "Product quantity updated successfully."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "404", description = "Not found."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "400", description = "Validation failed."),
            }
    )
    @PatchMapping("/{productId}/updateQuantity")
    public ResponseEntity<GenResponse<ProductResponse>> updateQuantity(@PathVariable UUID productId, @Valid @RequestBody ProductUpdateQuantityRequest productUpdateQuantityRequest){
        ProductResponse productResponse = productService.updateProductQuantity(productId, productUpdateQuantityRequest);
        return ResponseEntity.ok(GenResponse.of(productResponse));
    }

    @Operation(summary = "Delete a product by product id.",
            description = "### Requirements\n- bearer token must be provided",
            responses = {
                    @ApiResponse(content = @Content(mediaType = ""),
                            responseCode = "200", description = "Product deleted successfully."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "404", description = "Not found."),
            }
    )
    @DeleteMapping("/{productId}")
    public ResponseEntity<Void> delete(@PathVariable UUID productId){
        productService.deleteById(productId);
        return ResponseEntity.ok(null);
    }

    @Operation(summary = "Search products by order and quantity.",
            description =  "**Requirements**\n- bearer token must be provided\n" +
                    "\n- customerOrderId must be provided\n\n" +
                    "**Usage**\n- If provided only min than retrieves products that has greater quantities than min.\n" +
                    "\n- If provided only max than retrieves products that has less quantities than min.\n" +
                    "\n- If provided both min and max than retrieves products that has quantity between min and max.\n" +
                    "\n- If none of them are provided than retrieves all of the products belongs to the given order id.\n",
            parameters = {
                @Parameter(name = "min", in = ParameterIn.QUERY, required = false, description = "Represents min quantity."),
                @Parameter(name = "max", in = ParameterIn.QUERY, required = false, description = "Represents max quantity."),
            }
    )
    @GetMapping("/{customerOrderId}")
    public ResponseEntity<GenResponse<List<ProductResponse>>> searchProductsByQuantity(@PathVariable UUID customerOrderId,
                                                                                      @RequestParam(required = false) Integer min,
                                                                                      @RequestParam(required = false) Integer max){
        List<ProductResponse> productResponseList = productService.findAllByCustomerOrderAndQuantity(customerOrderId, min, max);
        return ResponseEntity.ok(GenResponse.of(productResponseList));
    }
}
