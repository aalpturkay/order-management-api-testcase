package com.aalpturkay.ordermanagementapi.common.exception;

import com.aalpturkay.ordermanagementapi.common.response.GenResponse;
import com.aalpturkay.ordermanagementapi.log.common.LogService;
import com.aalpturkay.ordermanagementapi.log.error.dto.LogErrorSaveRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.List;

@RestController
@ControllerAdvice
@Slf4j
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    @Qualifier("logErrorService")
    private LogService<LogErrorSaveRequest> logService;

    @ExceptionHandler
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest webRequest){
        Date errorDate = new Date();
        String message = ex.getMessage();
        String description = webRequest.getDescription(false);

        return getResponseEntity(errorDate, message, description, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleBusinessExceptions(BusinessException ex, WebRequest webRequest){
        Date errorDate = new Date();
        String message = ex.getBaseErrorMessage().getMessage();
        String description = webRequest.getDescription(false);

        return getResponseEntity(errorDate, message, description, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleItemNotFoundExceptions(ItemNotFoundException ex, WebRequest webRequest){

        Date errorDate = new Date();
        String message = ex.getBaseErrorMessage().getMessage();
        String description = webRequest.getDescription(false);

        return getResponseEntity(errorDate, message, description, HttpStatus.NOT_FOUND);
    }

    @NonNull
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex,
                                                                  @NonNull HttpHeaders headers,
                                                                  @NonNull HttpStatus status,
                                                                  @NonNull WebRequest request) {
        Date errorDate = new Date();
        String message = "Validation failed!";
        StringBuilder description = new StringBuilder();
        List<ObjectError> errorList = ex.getBindingResult().getAllErrors();
        if (!errorList.isEmpty()){
            for (ObjectError objectError : errorList) {
                String defaultMessage = objectError.getDefaultMessage();
                description.append(defaultMessage).append("\n");
            }
        } else {
            description = new StringBuilder(ex.getBindingResult().toString());
        }

        return getResponseEntity(errorDate, message, description.toString(), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> getResponseEntity(Date errorDate, String message, String description, HttpStatus internalServerError) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(errorDate, message, description);

        GenResponse<ExceptionResponse> genResponse = GenResponse.error(exceptionResponse);
        genResponse.setMessage(message);

        ResponseEntity<GenResponse<ExceptionResponse>> response = new ResponseEntity<>(genResponse, internalServerError);

        logError(response);

        return new ResponseEntity<>(genResponse, internalServerError);
    }

    private void logError(ResponseEntity<GenResponse<ExceptionResponse>> response){
        String errorMessageBody = getObjectString(response.getBody());
        String errorHeaders = getObjectString(response.getHeaders());

        LogErrorSaveRequest logErrorSaveRequest = LogErrorSaveRequest.builder()
                .httpStatus(response.getStatusCode())
                .headers(errorHeaders)
                .body(errorMessageBody)
                .build();

        logService.saveAndLog(logErrorSaveRequest);
    }

    private String getObjectString(Object object) {
        String errorMessageBody = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
            errorMessageBody = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return errorMessageBody;
    }
}
