package com.aalpturkay.ordermanagementapi.customerorder.entity;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntity;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.product.entity.Product;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "CUSTOMER_ORDERS")
@Getter
@Setter
public class CustomerOrder extends BaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Customer customer;

    @OneToMany(mappedBy = "customerOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Product> productList;

    @Column(name = "DELIVERY_ADDRESS")
    private String deliveryAddress;
}
