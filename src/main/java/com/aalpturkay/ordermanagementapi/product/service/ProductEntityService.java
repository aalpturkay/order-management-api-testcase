package com.aalpturkay.ordermanagementapi.product.service;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntityService;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.product.entity.Product;
import com.aalpturkay.ordermanagementapi.product.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductEntityService extends BaseEntityService<Product, ProductRepository> {

    public ProductEntityService(ProductRepository repository) {
        super(repository);
    }

    public List<Product> findAllByCustomerOrder(CustomerOrder customerOrder) {
        return getRepository().findAllByCustomerOrder(customerOrder);
    }

    public List<Product> findAllBetweenQuantityByCustomerOrder(int min, int max, CustomerOrder customerOrder){
        return getRepository().findAllBetweenQuantityByCustomerOrder(min, max, customerOrder);
    }

    public List<Product> findAllSmallerQuantity(int amount, CustomerOrder customerOrder){
        return getRepository().findAllSmallerQuantityByCustomerOrder(amount, customerOrder);
    }

    public List<Product> findAllGreaterQuantity(int amount, CustomerOrder customerOrder){
        return getRepository().findAllGreaterQuantityByCustomerOrder(amount, customerOrder);
    }
}
