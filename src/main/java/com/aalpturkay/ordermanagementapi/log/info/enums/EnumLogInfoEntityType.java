package com.aalpturkay.ordermanagementapi.log.info.enums;

public enum EnumLogInfoEntityType {
    PRODUCT,
    ORDER,
    CUSTOMER
    ;
}
