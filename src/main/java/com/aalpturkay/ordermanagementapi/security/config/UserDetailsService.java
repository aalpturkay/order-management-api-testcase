package com.aalpturkay.ordermanagementapi.security.config;

import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customer.service.CustomerEntityService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final CustomerEntityService customerEntityService;

    public UserDetailsService(CustomerEntityService customerEntityService) {
        this.customerEntityService = customerEntityService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerEntityService.findByUsername(username);
        return CustomerUserDetails.build(customer);
    }
}
