package com.aalpturkay.ordermanagementapi.customerorder.service;

import com.aalpturkay.ordermanagementapi.common.exception.ItemNotFoundException;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customer.service.CustomerService;
import com.aalpturkay.ordermanagementapi.customerorder.enums.EnumCustomerOrderErrorMessage;
import com.aalpturkay.ordermanagementapi.customerorder.converter.CustomerOrderConverter;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderResponse;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderSaveRequest;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderUpdateDeliveryAddressRequest;
import com.aalpturkay.ordermanagementapi.customerorder.entity.CustomerOrder;
import com.aalpturkay.ordermanagementapi.log.common.LogService;
import com.aalpturkay.ordermanagementapi.log.info.dto.LogInfoSaveRequest;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoCrudType;
import com.aalpturkay.ordermanagementapi.log.info.enums.EnumLogInfoEntityType;
import com.aalpturkay.ordermanagementapi.product.dto.ProductResponse;
import com.aalpturkay.ordermanagementapi.product.service.ProductService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {
    private final CustomerOrderEntityService customerOrderEntityService;
    private final CustomerOrderConverter customerOrderConverter;
    private final CustomerService customerService;
    private final ProductService productService;
    private final LogService<LogInfoSaveRequest> logService;

    public CustomerOrderServiceImpl(CustomerService customerService,
                                    CustomerOrderConverter customerOrderConverter,
                                    ProductService productService,
                                    CustomerOrderEntityService customerOrderEntityService,
                                    LogService<LogInfoSaveRequest> logService) {
        this.customerService = customerService;
        this.customerOrderConverter = customerOrderConverter;
        this.productService = productService;
        this.customerOrderEntityService = customerOrderEntityService;
        this.logService = logService;
    }

    @Override
    public CustomerOrderResponse save(CustomerOrderSaveRequest saveRequest, UUID customerId) {
        Customer customer = customerService.findById(customerId);
        CustomerOrder customerOrder = customerOrderConverter.convertToCustomerOrder(customer);

        customerOrderEntityService.save(customerOrder);
        List<ProductResponse> productResponseList = productService.saveAll(saveRequest.getProducts(), customerOrder);

        saveAndLogInfo(EnumLogInfoCrudType.SAVE);

        return customerOrderConverter.convertToCustomerOrderResponse(customerOrder, productResponseList);
    }

    @Override
    public List<CustomerOrderResponse> findAll(UUID customerId) {
        List<CustomerOrderResponse> customerOrderResponseList;
        Customer customer = customerService.findById(customerId);

        List<CustomerOrder> customerOrderList = customerOrderEntityService.findAllByCustomer(customer);

        customerOrderResponseList = customerOrderList.stream().map(customerOrder ->
                customerOrderConverter.
                        convertToCustomerOrderResponse(customerOrder, productService.
                                findAllByCustomerOrder(customerOrder))).collect(Collectors.toList());

        return customerOrderResponseList;
    }

    @Override
    public CustomerOrderResponse findById(UUID customerOrderId) {
        CustomerOrder customerOrder = getCustomerOrderByIdWithControl(customerOrderId);
        List<ProductResponse> productResponseList = productService.findAllByCustomerOrder(customerOrder);

        saveAndLogInfo(EnumLogInfoCrudType.GET);

        return customerOrderConverter.convertToCustomerOrderResponse(customerOrder, productResponseList);
    }

    @Override
    public void deleteById(UUID customerOrderId) {
        CustomerOrder customerOrder = getCustomerOrderByIdWithControl(customerOrderId);
        customerOrderEntityService.delete(customerOrder);

        saveAndLogInfo(EnumLogInfoCrudType.DELETE);
    }

    @Override
    public CustomerOrderResponse updateDeliveryAddress(UUID customerOrderId, CustomerOrderUpdateDeliveryAddressRequest updateDeliveryAddressRequest) {
        CustomerOrder customerOrder = getCustomerOrderByIdWithControl(customerOrderId);
        customerOrder.setDeliveryAddress(updateDeliveryAddressRequest.getDeliveryAddress());
        customerOrderEntityService.save(customerOrder);
        List<ProductResponse> productResponseList = productService.findAllByCustomerOrder(customerOrder);

        saveAndLogInfo(EnumLogInfoCrudType.UPDATE);

        return customerOrderConverter.convertToCustomerOrderResponse(customerOrder, productResponseList);
    }

    private CustomerOrder getCustomerOrderByIdWithControl(UUID customerOrderId) {
        return customerOrderEntityService.findById(customerOrderId)
                .orElseThrow(() -> new ItemNotFoundException(EnumCustomerOrderErrorMessage.ORDER_NOT_FOUND));
    }

    private void saveAndLogInfo(EnumLogInfoCrudType crudType){
        LogInfoSaveRequest logInfoSaveRequest = LogInfoSaveRequest.builder()
                .crudType(crudType)
                .entityType(EnumLogInfoEntityType.ORDER)
                .build();

        logService.saveAndLog(logInfoSaveRequest);
    }
}
