package com.aalpturkay.ordermanagementapi.customer.service;

import com.aalpturkay.ordermanagementapi.common.base.BaseEntityService;
import com.aalpturkay.ordermanagementapi.common.exception.EnumErrorMessage;
import com.aalpturkay.ordermanagementapi.common.exception.ItemNotFoundException;
import com.aalpturkay.ordermanagementapi.customer.entity.Customer;
import com.aalpturkay.ordermanagementapi.customer.repository.CustomerRepository;
import org.springframework.stereotype.Service;

@Service
public class CustomerEntityService extends BaseEntityService<Customer, CustomerRepository> {
    public CustomerEntityService(CustomerRepository repository) {
        super(repository);
    }

    public Customer findByUsername(String username) {
        return getRepository().findByUsername(username).
                orElseThrow(() -> new ItemNotFoundException(EnumErrorMessage.ITEM_NOT_FOUND));
    }

    public boolean existsByUsername(String username){
        return getRepository().existsByUsername(username);
    }
}
