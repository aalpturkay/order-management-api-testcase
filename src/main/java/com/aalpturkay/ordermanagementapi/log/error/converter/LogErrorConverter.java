package com.aalpturkay.ordermanagementapi.log.error.converter;

import com.aalpturkay.ordermanagementapi.log.error.dto.LogErrorSaveRequest;
import com.aalpturkay.ordermanagementapi.log.error.entity.LogError;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class LogErrorConverter {

    public LogError convertToLogError(LogErrorSaveRequest logErrorSaveRequest){
        LogError logError = new LogError();
        logError.setErrorDate(new Date());
        logError.setBody(logErrorSaveRequest.getBody());
        logError.setHttpStatus(logErrorSaveRequest.getHttpStatus());
        logError.setHeaders(logErrorSaveRequest.getHeaders());

        return logError;
    }
}
