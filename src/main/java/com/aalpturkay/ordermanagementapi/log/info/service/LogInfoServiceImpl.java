package com.aalpturkay.ordermanagementapi.log.info.service;

import com.aalpturkay.ordermanagementapi.log.common.LogService;
import com.aalpturkay.ordermanagementapi.log.info.converter.LogInfoConverter;
import com.aalpturkay.ordermanagementapi.log.info.dto.LogInfoSaveRequest;
import com.aalpturkay.ordermanagementapi.log.info.entity.LogInfo;
import com.aalpturkay.ordermanagementapi.security.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Qualifier("logInfoService")
@Slf4j
public class LogInfoServiceImpl implements LogService<LogInfoSaveRequest> {

    private final LogInfoConverter logInfoConverter;
    private final LogInfoEntityService logInfoEntityService;

    public LogInfoServiceImpl(LogInfoConverter logInfoConverter, LogInfoEntityService logInfoEntityService) {
        this.logInfoConverter = logInfoConverter;
        this.logInfoEntityService = logInfoEntityService;
    }

    @Override
    public void saveAndLog(LogInfoSaveRequest logSaveDto) {
        LogInfo logInfo = logInfoConverter.convertToLogInfo(logSaveDto);
        logInfo.setCustomerId(AuthenticationService.currentUserId());
        logInfo.setInfoDate(new Date());
        logInfo = logInfoEntityService.save(logInfo);

        log.info("Customer: {}, request {} action on {} entity at {}",
                logInfo.getCustomerId(), logInfo.getCrudType(),
                logInfo.getEntityType(), logInfo.getInfoDate());
    }
}
