package com.aalpturkay.ordermanagementapi.log.info.repository;

import com.aalpturkay.ordermanagementapi.log.info.entity.LogInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LogInfoRepository extends JpaRepository<LogInfo, UUID> {
}
