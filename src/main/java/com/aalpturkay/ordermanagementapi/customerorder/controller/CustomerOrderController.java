package com.aalpturkay.ordermanagementapi.customerorder.controller;

import com.aalpturkay.ordermanagementapi.common.response.GenResponse;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderResponse;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderSaveRequest;
import com.aalpturkay.ordermanagementapi.customerorder.dto.CustomerOrderUpdateDeliveryAddressRequest;
import com.aalpturkay.ordermanagementapi.customerorder.service.CustomerOrderService;
import com.aalpturkay.ordermanagementapi.security.service.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
public class CustomerOrderController {
    private final CustomerOrderService customerOrderService;

    public CustomerOrderController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @PostMapping
    @Operation(
            summary = "Create an order with the products.",
            description = "**Requirements**\n- bearer token must be provided" +
                    "\n- quantity must be positive" +
                    "\n- price must be positive or zero\n\n" +
                    "### Sample Input\n" +
                    "```\n" + "{\n   \"products\": [" +
                    "\n     {" +
                    "\n         \"barcode\": \"1111111111111\"," +
                    "\n         \"description\": \"Filter Coffee\"," +
                    "\n         \"quantity\": 15," +
                    "\n         \"price\": 420.99" +
                    "\n     }," +
                    "\n     {" +
                    "\n         \"barcode\": \"2222222222222\"," +
                    "\n         \"description\": \"Water\"," +
                    "\n         \"quantity\": 35," +
                    "\n         \"price\": 344.35" +
                    "\n     }" +
                    "\n   ]" +
                    "\n}" + "\n```\n\n" +
                    "### Sample Invalid Input\n" +
                    "```\n" + "{\n   \"products\": [" +
                    "\n     {" +
                    "\n         \"barcode\": \"1111111111111\"," +
                    "\n         \"description\": \"Filter Coffee\"," +
                    "\n         \"quantity\": 3.1," +
                    "\n         \"price\": -420.99" +
                    "\n     }," +
                    "\n     {" +
                    "\n         \"barcode\": \"2222222222222\"," +
                    "\n         \"description\": \"Water\"," +
                    "\n         \"quantity\": -5," +
                    "\n         \"price\": -100" +
                    "\n     }" +
                    "\n   ]" +
                    "\n}" + "\n```\n\n" +
                    "### Sample Output\n" +
                    "```\n" + "{\n" +
                    " \"data\": {\n" +
                    "      \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                    "\n      \"customerName\": \"Alp Turkay\"," +
                    "\n      \"deliveryAddress\": \"Buca/Izmir\",\n" +
                    "      \"products\": [" +
                    "\n         {" +
                    "\n             \"productId\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                    "\n             \"barcode\": \"1111111111111\"," +
                    "\n             \"description\": \"Filter Coffee\"," +
                    "\n             \"quantity\": 15," +
                    "\n             \"price\": 420.99" +
                    "\n         }," +
                    "\n         {" +
                    "\n             \"productId\": \"3ty54da-5717-4562-5233-1w453f35afa6\"," +
                    "\n             \"barcode\": \"2222222222222\"," +
                    "\n             \"description\": \"Water\"," +
                    "\n             \"quantity\": 35," +
                    "\n             \"price\": 344.35" +
                    "\n         }," +
                    "\n     ]" +
                    "\n   }" +
                    "\n \"success\": true," +
                    "\n \"message\": null" +
                    "\n}" + "\n```\n\n",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                    CustomerOrderResponse.class)), responseCode = "201", description = "Order successfully created."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
                    @ApiResponse(content = @Content(mediaType = "application/json"), responseCode = "400",
                            description = "Invalid input.")
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CustomerOrderSaveRequest.class),
                            examples = {
                                    @ExampleObject(
                                            name = "Sample Successful Input",
                                            summary = "Sample Input",
                                            value = "{\n   \"products\": [" +
                                                    "\n     {" +
                                                    "\n         \"barcode\": \"1111111111111\"," +
                                                    "\n         \"description\": \"Filter Coffee\"," +
                                                    "\n         \"quantity\": 15," +
                                                    "\n         \"price\": 420.99" +
                                                    "\n     }," +
                                                    "\n     {" +
                                                    "\n         \"barcode\": \"2222222222222\"," +
                                                    "\n         \"description\": \"Water\"," +
                                                    "\n         \"quantity\": 35," +
                                                    "\n         \"price\": 344.35" +
                                                    "\n     }" +
                                                    "\n   ]" +
                                                    "\n}"
                                    )
                            }
                    )
            )
    )
    public ResponseEntity<GenResponse<CustomerOrderResponse>> save(@Valid @RequestBody CustomerOrderSaveRequest saveRequest){
        UUID customerId = AuthenticationService.currentUserId();
        return new ResponseEntity<>(GenResponse.of(customerOrderService.save(saveRequest, customerId)), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all orders you've created.",
            description = "**Requirements**\n- bearer token must be provided\n" +
                    "### Sample Output\n" +
                    "```\n" + "{\n" +
                    "  \"data\": [\n" +
                    "    {\n" +
                    "     \"id\": \"ba87e669-0e12-4ceb-ab48-e4b3bb2b7970\",\n" +
                    "     \"customerName\": \"Alp Turkay\",\n" +
                    "     \"deliveryAddress:\": \"Buca/Izmir\",\n" +
                    "     \"products\": [" +
                    "\n      {" +
                    "\n         \"barcode\": \"1111111111111\"," +
                    "\n         \"description\": \"Filter Coffee\"," +
                    "\n         \"quantity\": 15," +
                    "\n         \"price\": 420.99" +
                    "\n      }" +
                    "\n     ]" +
                    "\n    }," +
                    "\n    {\n" +
                    "     \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\",\n" +
                    "     \"customerName\": \"Alp Turkay\",\n" +
                    "     \"deliveryAddress:\": \"Buca/Izmir\",\n" +
                    "     \"products\": [" +
                    "\n      {" +
                    "\n         \"barcode\": \"2222222222222\"," +
                    "\n         \"description\": \"Water\"," +
                    "\n         \"quantity\": 35," +
                    "\n         \"price\": 344.35" +
                    "\n      }" +
                    "\n     ]" +
                    "\n    }" +
                    "\n  ]," +
                    "\n  \"success\": true," +
                    "\n  \"message\": null" +
                    "\n}" + "\n```\n\n",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "200", description = "Orders fetched successfully."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
            }
    )
    @GetMapping
    public ResponseEntity<GenResponse<List<CustomerOrderResponse>>> getAll(){
        UUID currentCustomerId = AuthenticationService.currentUserId();
        return ResponseEntity.ok(GenResponse.of(customerOrderService.findAll(currentCustomerId)));
    }

    @Operation(summary = "Get one order by a given order id.",
    description = "**Requirements**\n- bearer token must be provided\n" +
            "### Sample Output\n" +
            "```\n" + "{\n" +
            " \"data\": {\n" +
            "      \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
            "\n      \"customerName\": \"Alp Turkay\"," +
            "\n      \"deliveryAddress\": \"Alsancak/Izmir\",\n" +
            "      \"products\": [" +
            "\n         {" +
            "\n             \"productId\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
            "\n             \"barcode\": \"1111111111111\"," +
            "\n             \"description\": \"Filter Coffee\"," +
            "\n             \"quantity\": 15," +
            "\n             \"price\": 420.99" +
            "\n         }," +
            "\n         {" +
            "\n             \"productId\": \"3ty54da-5717-4562-5233-1w453f35afa6\"," +
            "\n             \"barcode\": \"2222222222222\"," +
            "\n             \"description\": \"Water\"," +
            "\n             \"quantity\": 35," +
            "\n             \"price\": 344.35" +
            "\n         }," +
            "\n     ]" +
            "\n   }" +
            "\n \"success\": true," +
            "\n \"message\": null" +
            "\n}" + "\n```\n\n",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            CustomerOrderResponse.class)), responseCode = "200", description = "Order fetched successfully."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
            },
            parameters = {
                    @Parameter(name = "customerOrderId", in = ParameterIn.PATH, required = true, description = "UUID formatted" +
                            " data that belongs to customer's order.", schema = @Schema(type = "string", format = "uuid"))}
    )
    @GetMapping("/{customerOrderId}")
    public ResponseEntity<GenResponse<CustomerOrderResponse>> getOne(@PathVariable UUID customerOrderId){
        return ResponseEntity.ok(GenResponse.of(customerOrderService.findById(customerOrderId)));
    }

    @Operation(summary = "Delete an order by a given order id.",
            description = "**Requirements**\n- bearer token must be provided\n",
            responses = {
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "200", description = "Order deleted successfully."),
                    @ApiResponse(content = @Content(mediaType = "application/json"),
                            responseCode = "401", description = "Unauthorized."),
            }
    )
    @DeleteMapping("/{customerOrderId}")
    public ResponseEntity<Void> delete(@PathVariable UUID customerOrderId){
        customerOrderService.deleteById(customerOrderId);
        return ResponseEntity.ok(null);
    }

    @Operation(summary = "Update delivery address an order by a given order id.",
    description = "**Requirements**\n- bearer token must be provided\n" +
            "\n- Updates the delivery address with a provided delivery address data for a particular order.\n\n" +
            "### Sample Input\n" +
            "```\n" + "{\n" +
            " \"deliveryAddress\": \"Alsancak/Izmir\"" +
            "\n}" + "\n```\n\n" +
            "### Sample Output\n" +
            "```\n" + "{\n" +
            " \"data\": {\n" +
            "      \"id\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
            "\n      \"customerName\": \"Alp Turkay\"," +
            "\n      \"deliveryAddress\": \"Alsancak/Izmir\",\n" +
            "      \"products\": [" +
            "\n         {" +
            "\n             \"productId\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
            "\n             \"barcode\": \"1111111111111\"," +
            "\n             \"description\": \"Filter Coffee\"," +
            "\n             \"quantity\": 15," +
            "\n             \"price\": 420.99" +
            "\n         }," +
            "\n         {" +
            "\n             \"productId\": \"3ty54da-5717-4562-5233-1w453f35afa6\"," +
            "\n             \"barcode\": \"2222222222222\"," +
            "\n             \"description\": \"Water\"," +
            "\n             \"quantity\": 35," +
            "\n             \"price\": 344.35" +
            "\n         }," +
            "\n     ]" +
            "\n   }" +
            "\n \"success\": true," +
            "\n \"message\": null" +
            "\n}" + "\n```\n\n",
    parameters = {
            @Parameter(name = "customerOrderId", in = ParameterIn.PATH, required = true, description = "UUID formatted" +
                    " data that belongs to customer's order.", schema = @Schema(type = "string", format = "uuid"))
    })
    @PatchMapping("/{customerOrderId}/deliveryAddress")
    public ResponseEntity<GenResponse<CustomerOrderResponse>> changeDeliveryAddress(@PathVariable UUID customerOrderId, @Valid @RequestBody CustomerOrderUpdateDeliveryAddressRequest deliveryAddressRequest){
        return ResponseEntity.ok(GenResponse.of(customerOrderService.updateDeliveryAddress(customerOrderId, deliveryAddressRequest)));
    }
}
