package com.aalpturkay.ordermanagementapi.customerorder.dto;

import com.aalpturkay.ordermanagementapi.product.dto.OrderProductSaveRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CustomerOrderSaveRequest {
    @NotNull(message = "Product List must be provided!")
    private List<OrderProductSaveRequest> products;
}
