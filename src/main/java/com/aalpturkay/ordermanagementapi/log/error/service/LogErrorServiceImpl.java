package com.aalpturkay.ordermanagementapi.log.error.service;

import com.aalpturkay.ordermanagementapi.log.common.LogService;
import com.aalpturkay.ordermanagementapi.log.error.converter.LogErrorConverter;
import com.aalpturkay.ordermanagementapi.log.error.dto.LogErrorSaveRequest;
import com.aalpturkay.ordermanagementapi.log.error.entity.LogError;
import com.aalpturkay.ordermanagementapi.security.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("logErrorService")
@Slf4j
public class LogErrorServiceImpl implements LogService<LogErrorSaveRequest> {

    private final LogErrorConverter logErrorConverter;
    private final LogErrorEntityService logErrorEntityService;

    public LogErrorServiceImpl(LogErrorConverter logErrorConverter, LogErrorEntityService logErrorEntityService) {
        this.logErrorConverter = logErrorConverter;
        this.logErrorEntityService = logErrorEntityService;
    }

    @Override
    public void saveAndLog(LogErrorSaveRequest logErrorSaveRequest) {
        LogError logError = logErrorConverter.convertToLogError(logErrorSaveRequest);
        logError.setCustomerId(AuthenticationService.currentUserId());

        logError = logErrorEntityService.save(logError);

        log.error(logError.getBody());
    }
}
